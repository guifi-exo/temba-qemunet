#!/bin/bash
set -x

TUNTAPUP(){
  # src https://wiki.debian.org/QEMU
  ip tuntap add dev $1 mode tap group netdev
  ip link set $1 up
}

# src http://wiki.bash-hackers.org/syntax/expansion/brace#zero_padded_number_expansion
ts=(tap{0..7})
switch="br0"

ip link add name $switch type bridge

#for t in $ts; do
for t in ${ts[@]}; do

  TUNTAPUP $t
  # src http://www.linux-kvm.org/page/Networking
  ip link set $t master $switch

  # allow routing traffic to internet
  iptables -I FORWARD 1 -i $t -j ACCEPT
  iptables -I FORWARD 1 -o $t -m state --state RELATED,ESTABLISHED -j ACCEPT

done

# DEBUG FORWARD
## iptables --policy FORWARD ACCEPT
## iptables --policy FORWARD DROP

iptables -I FORWARD 1 -i $switch -j ACCEPT
iptables -I FORWARD 1 -o $switch -m state --state RELATED,ESTABLISHED -j ACCEPT

iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE

# helper:
#  brctl show

ip a a 192.168.99.1/24 dev $switch
ip link set $switch up
dnsmasq --interface=$switch --dhcp-range=192.168.99.5,192.168.99.12,12h
