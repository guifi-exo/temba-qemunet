#!/bin/bash
set -x

TUNTAPDOWN(){
  # src https://wiki.debian.org/QEMU
  ip link set $1 down
  ip tuntap del dev $1 mode tap
}

ts=(tap{0..7})
switch="br0"

ip link del $switch

for t in ${ts[@]}; do

  # src https://unix.stackexchange.com/a/255489
  ip link del $t
  # TUNTAPDOWN $t
  # # src http://www.linux-kvm.org/page/Networking
  # ip link set $t nomaster $switch 


done

iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE
iptables -t nat -D POSTROUTING -o wlan0 -j MASQUERADE

killall dnsmasq
